#!/usr/bin/env bash

set -e

mkdir build
cd build
cmake \
  -D CMAKE_BUILD_TYPE=Release     \
  -D CMAKE_INSTALL_PREFIX=$PREFIX \
  -D CMAKE_INSTALL_LIBDIR=lib     \
  -D CIFTILIB_USE_XMLPP=1         \
  ..
make VERBOSE=1
make install
